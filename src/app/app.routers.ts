
import { RouterModule,Routes } from "@angular/router";
import { InicioComponent } from "./component/inicio/inicio.component";
import { MenuComponent } from "./component/menu/menu.component";
import { ContactosComponent } from "./component/contactos/contactos.component";
import { Opcion1Component } from "./component/opcion1/opcion1.component";
import { Opcion2Component } from "./component/opcion2/opcion2.component";
import { Opcion3Component } from "./component/opcion3/opcion3.component";
import { Opcion4Component } from "./component/opcion4/opcion4.component";
import { Opcion5Component } from "./component/opcion5/opcion5.component";

const APP_ROUTES: Routes = [
     { path: 'inicio', component: InicioComponent },
     { path: 'menu', component: MenuComponent},
     { path: 'contactos', component: ContactosComponent},
     { path: 'opcion1', component: Opcion1Component},
     { path: 'opcion2', component: Opcion2Component},
     { path: 'opcion3', component: Opcion3Component},
     { path: 'opcion4', component: Opcion4Component},
     { path: 'opcion5', component: Opcion5Component},
     { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
    ];
    export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
    